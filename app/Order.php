<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Throwable;

class Order extends Model
{
    protected $fillable = ['date', 'shop'];

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Buyer::class, 'client_id', 'id');
    }

    public function createItems(array $orderItems)
    {
        foreach ($orderItems as $item) {

            $discount = $item['productDiscount'];
            $product = Product::find($item['productId']);

            $orderItem = new OrderItem([
                'quantity' => $item['productQty'],
                'discount' => $discount,
                'price' => $product->price,
                'sum' => round((1 - $discount / 100) * $product->price * $item['productQty'])
            ]);

            $orderItem->product()->associate($product);
            $this->orderItems()->save($orderItem);
        }
    }

    public function updateOrder(array $data)
    {
        try {
            DB::beginTransaction();
            $this->orderItems()->delete();
            $orderItems = $data['orderItems'];
            $this->createItems($orderItems);
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            return false;
        }

        return true;
    }

    public static function create(array $data)
    {
        try {
            DB::beginTransaction();
            $buyer = Buyer::find($data['buyerId']);

            $order = new static([
                'date' => Carbon::now()
            ]);

            $order->client()->associate($buyer);
            $order->save();
            $orderItems = $data['orderItems'];
            $order->createItems($orderItems);
            DB::commit();
        } catch (Throwable $e) {
            DB::rollBack();
            return false;
        }

        return $order->id;
    }
}
