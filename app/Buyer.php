<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $fillable = ['name', 'surname', 'city', 'country', 'addressLine', 'phone'];

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'client_id', 'id');
    }
}
