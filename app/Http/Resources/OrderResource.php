<?php

namespace App\Http\Resources;

use App\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $collection =  OrderItemResource::collection($this->orderItems);

        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $collection->sum('sum') / 100,
            'orderItems' => $collection,
            'buyer' => new BuyerResource($this->client)
        ];
    }
}
