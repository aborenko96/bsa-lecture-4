<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Order;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['order'], ['only' => ['show', 'destroy']]);
        $this->middleware(['buyer', 'order.items'], ['only' => ['store']]);
        $this->middleware(['order', 'order.items'], ['only' => ['update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();

        return response()->json($orders->map(function ($order) {
            return new OrderResource($order);
        }))->setEncodingOptions(JSON_PRETTY_PRINT);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $result = Order::create($data);

        return response($result ? [
            'result' => 'success',
            'message' => "New order id is $result"
        ] : [
            'result' => 'fail'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()
            ->json(new OrderResource(Order::find($id)))
            ->setEncodingOptions(JSON_PRETTY_PRINT);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $order = Order::find($id);

        $result = $order->updateOrder($data);

        return response(['result' => $result ? 'success' : 'fail']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Order::destroy($id);

        return response(['result' => $result ? 'success' : 'fail']);
    }
}
