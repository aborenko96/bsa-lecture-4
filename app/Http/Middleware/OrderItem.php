<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class OrderItem
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'orderItems' => 'required|array|min:1',
        ]);

        if ($validator->fails()) {
            return response([
                'result' => 'fail',
                'message' => 'No order items'
            ]);
        }

        $orderItems = $data['orderItems'];

        foreach ($orderItems as $item) {
            $validator = Validator::make($item, [
                'productId' => 'required|exists:my_products,id',
                'productQty' => 'required|numeric|min:1',
                'productDiscount' => 'numeric|min:0|max:100'
            ]);

            if ($validator->fails()) {
                return response([
                    'result' => 'fail',
                    'message' => $validator->errors()
                ]);
            }
        }

        return $next($request);
    }
}
