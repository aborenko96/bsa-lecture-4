<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Validator;

class Buyer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'buyerId' => 'required|exists:buyers,id'
        ]);

        if ($validator->fails()) {
            return response([
                'result' => 'fail',
                'message' => $validator->errors()
            ]);
        }

        return $next($request);
    }
}
