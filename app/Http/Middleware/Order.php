<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;

class Order
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $params = $request->route()->parameters();

        $validator = Validator::make($params, [
            'order' => 'required|exists:orders,id',
        ]);

        if ($validator->fails()) {
            return response([
                'result' => 'fail',
                'message' => $validator->errors()
            ]);
        }

        return $next($request);
    }
}
