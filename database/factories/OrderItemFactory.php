<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\{
    OrderItem, Product
};
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'quantity' => $faker->numberBetween(10, 50),
        'discount' => $faker->numberBetween(0, 100)
    ];
});
