<?php

use App\{
    Order, Seller, OrderItem, Product, Buyer
};
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        $buyers = factory(\App\Buyer::class, 20)->create();

        factory(Order::class, 100)->make()->each(function (Order $order) use ($products, $buyers) {

            $order->client()->associate($buyers->random());
            $order->push();

            $order->orderItems()
                ->saveMany(
                    factory(OrderItem::class, rand(1, 10))
                        ->make(['order_id' => $order->id])
                        ->each(function (OrderItem $orderItem) use ($products) {

                            $product = $products->random() ?? factory(Product::class)
                                    ->create(['seller_id' => factory(Seller::class)->create()->id]);

                            $orderItem->price = $product->price;
                            $orderItem->sum = round((1 - $orderItem->discount / 100) * $orderItem->quantity * $product->price);

                            $orderItem->product()->associate($product);

                            $orderItem->push();
                        })
                );
        });
    }
}
